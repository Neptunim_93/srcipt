# disableVm.ps1 vcenter vmName1, vmName2
param
(
  [string]$server,
  [string[]]$names
)
Write-Host "Starting the disableVm script..."
Connect-VIServer $server | Out-Null
foreach ($name in $names)
{
  $vm = Get-VM $name
  
  if ($vm.PowerState -eq "PoweredOn")
  {
    Stop-VM -VM $vm -Confirm:$false | Out-Null
    Write-Host "Stopping virtual machine: $name"
  }
  
  $vm = Get-VM $name
  $folders =  Get-Inventory -Name "Disabled"
  $clusterVm = $vm.Folder.Parent.Parent.Name
  $disableDate = Get-Date
  $deleteDate = $disableDate.AddMonths(3)
  $vmNotes = $vm.Notes
  $note = "le $disableDate : Extinction Suppression OK le $deleteDate `n$vmNotes"
  Set-VM -VM $vm -Description $note -Confirm:$false | Out-Null
  Write-Host "Setting note for virtual machine: $name"
  foreach ($folder in $folders)
  {
    $clusterFolder = $folder.Parent.Parent.Name
    if ($clusterFolder -eq $clusterVm)
    {
      Move-VM -VM $vm -InventoryLocation $folder -Confirm:$false | Out-Null
      Write-Host "Moving virtual machine: $name to folder: $folder"
    }
  }
}
Disconnect-VIServer $server -Confirm:$false | Out-Null
Write-Host "disableVm script completed."