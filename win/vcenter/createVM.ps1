param
(
  [string]$server = ""
)

Connect-VIServer -Server $server

$template= Get-Template -name ""
$newVMName = ""
$clusterName = ""
$cluster = Get-Cluster "CLUSTER" 
$resourcePool = Get-Cluster "CLUSTER" | Get-ResourcePool 
$datastoreName = ""
$datastore = Get-Datastore -Name $datastoreName
$folderName = ""
$Custom = Get-OSCustomizationSpec -Name ""
$ipaddress = ""
$mask = ""
$gateway = ""
$dns1 = ""
$dns2 = ""
$folders = Get-Folder -Name $folderName -Type VM


foreach ($folder in $folders)
{
  $clusterFolder = $folder.Parent.Parent.Name
  if ($clusterFolder -eq $clusterName)
  {
	  $vmFolder = $folder
  }
}
Get-OSCustomizationSpec $Custom | Get-OSCustomizationNicMapping | Set-OSCustomizationNicMapping -IpMode UseStaticIp -IpAddress $ipaddress -SubnetMask $mask -DefaultGateway $gateway -Dns $dns1,$dns2

New-VM -Name $newVMName -Template $template -ResourcePool $resourcePool -Location $vmFolder -Datastore $datastore -OSCustomizationSpec $Custom
$NewVM = Get-VM -Name $newVMName
$NewVM
$NewVM | Start-VM
$NewVM = Get-VM -Name $newVMName
$NewVM
Disconnect-VIServer -Confirm:$false