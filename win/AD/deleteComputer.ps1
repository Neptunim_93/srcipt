# deleteComputer.ps1 -PATH OUPATH
param (
  [string]$PATH
)

$objects = Get-ADObject -Filter * -SearchBase "$PATH" -Properties Description

foreach ($object in $objects)
{
  $description = $object.Description
  if ($description -ne $null)
  {
    $dates = [Regex]::Matches($description, 'delete (\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2})')
    foreach ($dateMatch in $dates)
    {
      $dateSuppression = [DateTime]::ParseExact($dateMatch.Groups[1].Value, 'MM/dd/yyyy HH:mm:ss', $null)
      if ($dateSuppression -lt (Get-Date))
      {
        Write-Host "The deletion date $deletionDate has passed. Deleting object $($object.Name)."
        Remove-ADObject -Identity $object.DistinguishedName -Confirm:$false
        Write-Host "The object has been deleted."
      } 
      else
      {
        Write-Host "The deletion date $deletionDate has not yet passed for object $($object.Name)."
      }
    }
  }
  else
  {
    Write-Host "No description is defined for object $($object.Name)."
  }
}