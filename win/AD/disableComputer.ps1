# disableComputer.ps1 -name name
param (
  [string]$name
)

$computer = Get-ADComputer -Filter {Name -eq $name}
if ($computer)
{
  $computerDistinguishedName = $computer.DistinguishedName
  Disable-ADAccount -Identity $computerDistinguishedName
  $computer = Get-ADComputer -Filter {Name -eq $name}
  if ($computer.Enabled)
  {
    Write-Host "deactivation to fail for $name"
  }
  else
  {
    $disableDate = Get-Date
    $deleteDate = $disableDate.AddMonths(3)
    $commentaire = "disable $disableDate; delete $deleteDate"
    Set-ADObject -Identity $computerDistinguishedName -Description $commentaire
    Write-Host "désactivation le $disableDate; suppression OK le $deleteDate"
    Write-Host "disabling $name"
    $computerDistinguishedNameArray= $computerDistinguishedName -split ","
    $computerDistinguishedNameArray[1] = "OU=DISABLED"
    $computerDisablesPath = $computerDistinguishedNameArray[1..($computerDistinguishedNameArray.Length-1)] -join ","
    Move-ADObject -Identity $computerDistinguishedName -TargetPath $computerDisablesPath
    Write-Host "move $compterName to $computerDisablesPath"
  }
}
else
{
  Write-Host "The $name computer does not exist in Active Directory."
}