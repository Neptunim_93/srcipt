# dump.ps1 -OUs "OUPATH", "OUPATH2"
param (
    [string[]]$OUs
)

Import-Module ActiveDirectory
$OutputEncoding = [console]::OutputEncoding = [System.Text.Encoding]::UTF8

$users = foreach ($OU in $OUs) {
	Get-ADUser -Filter * -SearchBase $OU -Properties *
}

$exportPath = ""

$exportData = foreach ($user in $users) {
    Write-Host "Adresse de messagerie: $($user.EmailAddress)"
    [PSCustomObject]@{
        "Prenom"               = $user.GivenName
        "Nom"                  = $user.Surname
        "Description"          = $user.Description
        "Bureau"               = $user.Office
        "NumeroTelephone"  = $user.TelephoneNumber
        "AdresseMessagerie" = $user.EmailAddress
        "Fonction"             = $user.Title
        "Service"              = $user.Department
        "Societe"              = $user.Company
        "Telephone mobile"     = $user.MobilePhone
        "distinguishedName"    = $user.distinguishedName
    }
}

$exportData | Export-Csv -Path $exportPath -NoTypeInformation -Encoding UTF8

Write-Host "Export terminé vers $exportPath" 