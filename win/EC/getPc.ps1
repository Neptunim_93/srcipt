. ".\login.ps1"
function Get-ComputersInventory {
    param (
        [string]$AuthToken = "",
        [string]$baseUrl = "/api/1.4",
        [string]$NameFilter
    )
    $apiEndpoint = $baseUrl + "/inventory/scancomputers"

    $headers = @{
        "Authorization" = $AuthToken
    }
    $filters = @{
        namefilter = $NameFilter
    }
    # Construct URI with filters
    $uri = "$apiEndpoint"
    try {
        $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $headers
		Write-Host $response.message_response.page
		Write-Host $response.message_response.limit
        # Return the hardware list
        return $response.message_response.scancomputers
    } catch {
        Write-Error "Error: $($_.Exception.Message)"
    }
}

# Example usage
#$authToken = Get-AuthToken
#$hardwareList = Get-HardwareInventory -AuthToken $authToken -NameFilter "BASEATAL"
$computers = Get-ComputersInventory -NameFilter "BASEATAL"


#foreach ($computer in $computers)
#{
#	Write-Host $computer
#	if ($computer.resource_name -eq "BASEATAL")
#	{
#		Write-Host $computer
#	}
#}
# Process the hardware list