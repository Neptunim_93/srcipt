function Get-AuthToken {
    param
    (
        [string]$baseUrl = "/api/1.4",
        [string]$authType = "ad_authentication",
        [string]$domainName = "cucm"
    )
    $url = $baseurl + "/desktop/authentication"

    
    $credentials = Get-Credential -Message "Enter your AD credentials"
    
    $requestBody = @{
        "username"   = $credentials.UserName
        "password"   = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($credentials.GetNetworkCredential().Password))
        "auth_type"  = $authType
        "domainName" = $domainName
    } | ConvertTo-Json
    
    $headers = @{
        "Content-Type" = "application/json"
    }
    
    try {
        $response = Invoke-RestMethod -Uri $url -Method Post -Headers $headers -Body $requestBody
        $status = $response.status
    
        if ($status -eq "success") {
            $authData = $response.message_response.authentication
            if ($authData.two_factor_data -ne $null) {
                $uniqueUserID = $authData.two_factor_data.unique_userID
                $message = $authData.two_factor_data.message
                $otpValidationRequired = $authData.two_factor_data.is_TwoFactor_Enabled
    
                Write-Output "Two-factor authentication is enabled. Message: $message"
    
                if ($otpValidationRequired) {
                    $otpCredentials = Get-Credential -Message "Enter your OTP" -UserName $uniqueUserID
    
                    $otpValidationBody = @{
                        "uid"                 = $uniqueUserID
                        "otp"                 = $otpCredentials.GetNetworkCredential().Password
                        "rememberme_enabled" = "true"
                    } | ConvertTo-Json
    
                    $url = $baseurl + "/desktop/authentication/otpValidate"
                    $otpValidationResponse = Invoke-RestMethod -Uri $url -Method Post -Headers $headers -Body $otpValidationBody
                    $authToken = $otpValidationResponse.message_response.authentication.auth_data.auth_token
                    Write-Output "Authentication successful. Auth Token: $authToken"
                    return $authToken
                }
            } else {
                $authToken = $authData.auth_token
                Write-Output "Authentication successful. Auth Token: $authToken"
                return $authToken
            }
        } else {
            Write-Error "Authentication failed. Status: $status"
        }
    } catch {
        Write-Error "Error: $($_.Exception.Message)"
    }
}