[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$accessKey = ""
$secretKey = ""
$organizationId = "{organizationId}"
$url = "https://api.radar.f-secure.com/api/integration/authenticationcheck"
$headers = @{
    "Content-Type" = "application/json"
    "ApiAccessKey" = "{ApiAccessKey}"
    "ApiSecretKey" = "{ApiSecretKey}"
}
$response = Invoke-RestMethod -Uri $url -Headers $headers -Method Get

$response
